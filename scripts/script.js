let editMode = false;
const url = '/api/chat/message';

getAndRenderAllMessages();

setInterval(() => {
    findAndRenderNewMessages();
}, 2000);

function getMessageById(id) {
    $.get(`${url}/${id}`, res => {
        $(document).ready(() => {
            $("#username").val(res.username);
            $("#text").val(res.text);
            $("#put-returned-id").text(res.id);
        });
    }).fail(err => {
        console.error(err);
    });
}

function getAndRenderAllMessages() {
    $.get(`${url}`, res => {
        res.slice(0)
            .reverse()
            .map(message => {
                addMessageToDom(message.username, message.text, message.id);
            });
    }).fail(err => {
        console.error(err);
    });
}

function findAndRenderNewMessages() {
    let idPool = []; // All ids that we got after GET request
    $.get(`${url}`, res => {
        res.slice(0)
            .reverse()
            .map(message => {
                // If there are some new messages that weren't
                // previously rendered, we rendering them here
                if ($(`#id-${message.id}`).length == 0) { 
                    addMessageToDom(message.username, message.text, message.id);
                }
                if (!idPool.includes(message.id)) {
                    idPool.push(message.id);
                }
            });
        // If there were messages that have been already deleted from "database"
        // but haven't been deleted from our page, we deleting them here
        $(`.id`).each(function() {
            if (!idPool.includes(JSON.parse($(this).text()))) { 
                $(`#id-${$(this).text()}`).remove();
            }
        });
    }).fail(err => {
        console.error(err);
    });
}

function sanitize(html) {
    return $($.parseHTML(html)).text();
}

function addMessageToDom(username, text, id) {
    username = sanitize(username);
    text = sanitize(text);
    let messageArea = $(`#list`);
    let li = document.createElement("li");
    li.classList.add("message");
    li.id = `id-${id}`;

    let bold = document.createElement("b");
    let span1 = document.createElement("span");
    span1.classList.add("usr-name");
    span1.appendChild(document.createTextNode(`${username}`));
    bold.appendChild(span1);
    li.appendChild(bold);

    li.appendChild(document.createTextNode(": "));
    let span2 = document.createElement("span");
    span2.classList.add("txt");
    span2.appendChild(document.createTextNode(`${text}`));
    li.appendChild(span2);

    let span3 = document.createElement("span");
    span3.classList.add("id");
    span3.setAttribute("style","display:none");
    span3.appendChild(document.createTextNode(`${id}`));
    li.appendChild(span3);

    messageArea.append(li);
}

function sendMessage() {
    $(document).ready(() => {
        username = $("#username").val();
        text = $("#text").val();
        postMessage(username, text);
        $("#text").val("");
    });
}

$(document).ready(() => {
    $("#send").click(e => {
        e.preventDefault();
        if (editMode) {
            sendEditedMessage();
            editMode = false;
        } else {
            sendMessage();
        }
    });
});

function postMessage(username, text) {
    if(username === "" || text === "")
        return;
    $.ajax({
        type: "POST",
        url: `${url}`,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            username: username,
            text: text
        }),
        success: res => {
            if(res) {
                findAndRenderNewMessages();
            }
        },

        error: err => {
            console.error(err);
        }
    });
}

function sendEditedMessage() {
    username = $("#username").val();
    text = $("#text").val();
    id = $("#put-returned-id").text();
    putMessage(username, text, id);

    let element = $(`#id-${id}`);
    element.toggleClass("active");
    setTimeout(() => {
        element.toggleClass("active");
    }, 2000);

    $("#username").val("");
    $("#text").val("");
    $("#put-returned-id").text("");
}

function checkWhatClicked(element) {
    // If we clicked on a text or on a username and not on the free area of a message
    // we should go one parent back in case of text
    // and two parents back in case of username to get to the <li> element.
    // If we clicked on an empty space then return false and consequently stop the function 
    if (element.attr("class") === "txt") {
        return element.parent();
    } else if (element.attr("class") === "usr-name") {
        return element.parent().parent();
    } else if (element.attr("id") === "list") {
        return false;
    } else {
        return element;
    }
}

function editMessage(element) {
    editMode = true;
    element = checkWhatClicked(element);
    if (!element)
        return;
    $("#edit").off();
    let id = parseInt(element.find(".id").text());
    getMessageById(id);
}

function putMessage(username, text, id) {
    $.ajax({
        type: "PUT",
        url: `${url}/${id}`,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            username: username,
            text: text
        }),
        success: res => {
            let message = $(`#id-${id}`);
            message.find(`.usr-name`).html(res.username);
            message.find(`.txt`).html(res.text);
        },

        error: err => {
            console.error(err);
        }
    });
}

function removeMessageFromDOM(element) {
    element = checkWhatClicked(element);
    if (!element)
        return;
    $("#delete").off();
    let id = parseInt(element.find(".id").text());
    element.remove();
    deleteMessage(id);
}

function deleteMessage(id) {
    $.ajax({
        url: `${url}/${id}`,
        type: "DELETE",
        data: id,
        error: err => {
            console.error(err);
        }
    });
}

$(document).ready(() => {
    let menu = $(".menu");
    let messageList = $("#message-list");

    messageList.contextmenu(event => {
        element = $(event.target);
        addMenuClickEvents(element);
    });

    messageList.contextmenu(showMenu);
    menu.mouseleave(hideMenu);
});

function showMenu(e) {
    let menu = $(".menu");
    e.preventDefault();
    menu.css({ top: `${e.clientY - 20}px` });
    menu.css({ left: `${e.clientX - 20}px` });
    menu.css({ display: "block" });
}

function hideMenu() {
    let menu = $(".menu");
    menu.css({ display: "none" });
}

function addMenuClickEvents(element) {
    $("#delete").click(() => {
        removeMessageFromDOM(element);
    });

    $("#edit").click(() => {
        editMessage(element);
    });
}
